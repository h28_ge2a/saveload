#include <stdio.h>

#include <stdlib.h>
#include <time.h>

#include <fstream>	//fstream型を使うのに必要

using namespace std;	//「std::」を省略できるように

void main()
{
	//テキトーなデータ
	srand(time(NULL));
	int a[3];
	for (int i = 0; i < 3; i++)
	{
		a[i] = rand() % 100;
	}



	///////ここから保存/////////

	//作った（開いた）ファイルを入れる変数
	fstream fs;

	//ファイルを作成（既にある場合はそれを開く）
	//第１引数：ファイル名
	//第２引数：「ios::out」保存用　「ios::binary」バイナリモード
	fs.open("..\\test.data", ios::out | ios::binary);

	//作れなかった場合
	if (!fs.is_open()) 
	{
		printf("エラー\n");
		return;
	}

	//３つのデータを書き込む
	for (int i = 0; i < 3; i++)
	{
		//さっき作ったファイルにデータを書き込む
		//第１引数：保存したいデータのアドレス
		//第２引数：保存したいデータのサイズ
		fs.write((const char*)&a[i], sizeof(int));
	}

	//ファイルを閉じる
	fs.close();


	///////ここまで//////////







	for (int i = 0; i < 3; i++)
	{
		printf("%d\n", a[i]);
	}
	printf("を保存しました\n");
}