#include <stdio.h>
#include <fstream>	//fstream型を使うのに必要

using namespace std;	//「std::」を省略できるように

void main()
{
	//データを入れる変数
	int a[3];




	///////ここから開く/////////

	//開いたファイルを入れる変数
	fstream fs;

	//ファイルを開く
	//第１引数：ファイル名
	//第２引数：「ios::in」ロードする　「ios::binary」バイナリモード
	fs.open("..\\test.data.", ios::in | ios::binary);

	//開けなかった場合
	if (!fs.is_open())
	{
		printf("エラー\n");
		return;
	}

	//３つのデータを読み込む
	for (int i = 0; i < 3; i++)
	{
		//さっき開いたファイルからデータを読み込む
		//第１引数：読んだデータを入れたい変数のアドレス
		//第２引数：読みたいデータのサイズ
		fs.read((char*)&a[i], sizeof(int));
	}

	//ファイルを閉じる
	fs.close();

	///////ここまで/////////





	for (int i = 0; i < 3; i++)
	{
		printf("%d\n", a[i]);
	}
}