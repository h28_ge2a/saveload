#include <windows.h>
#include <stdio.h>

void main()
{
	//データを入れる変数
	int a[3];




	///////ここから開く/////////


	//ファイルハンドルを入れる変数
	HANDLE hFile;

	//ファイルを開く
	//第１引数：ファイル名
	//第２引数：保存するときは「GENERIC_WRITE」、開くときは「GENERIC_READ」
	//第５引数：「OPEN_EXISTING 」だとファイルが無ければエラー。
	//他の引数：知らん
	//戻り値：開いたファイルのハンドル
	hFile = CreateFile(L"../test.data", GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	//開けなかった場合
	if (hFile == INVALID_HANDLE_VALUE)
	{
		printf("エラー");
		return;
	}

	//開く場所（何バイト目から読むか）
	DWORD index = 0;

	//３つのデータを読み込む
	for (int i = 0; i < 3; i++)
	{
		//さっき開いたファイルからデータを読み込む
		//第１引数：ファイルハンドル
		//第２引数：読んだデータを入れたい変数のアドレス
		//第３引数：読みたいデータのサイズ
		//第４引数：読む場所（読んだサイズが自動的に加算される）
		//第５引数：知らん
		ReadFile(hFile, &a[i], sizeof(int), &index, NULL);
	}


	//ファイルを閉じる
	CloseHandle(hFile);

	///////ここまで/////////





	for (int i = 0; i < 3; i++)
	{
		printf("%d\n", a[i]);
	}
}