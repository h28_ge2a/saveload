#include <stdio.h>

#include <windows.h>
#include <time.h>

void main()
{
	//テキトーなデータ
	srand(time(NULL));
	int a[3];
	for (int i = 0; i < 3; i++)
	{
		a[i] = rand() % 100;
	}




	///////ここから保存/////////


	//ファイルハンドルを入れる変数
	HANDLE hFile;

	//ファイルを作成（既にある場合はそれを開く）
	//第１引数：ファイル名
	//第２引数：保存するときは「GENERIC_WRITE」、開くときは「GENERIC_READ」
	//第５引数：「CREATE_ALWAYS」だとファイルが無ければ新規に作り、ある場合は上書き。
	//他の引数：知らん
	//戻り値：作った（開いた）ファイルのハンドル
	hFile = CreateFile(L"../test.data", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	//作れなかった場合
	if (hFile == INVALID_HANDLE_VALUE) 
	{
		printf("エラー");
		return ;
	}

	//保存する場所（何バイト目に保存するか）
	DWORD index = 0;

	//３つのデータを書き込む
	for (int i = 0; i < 3; i++)
	{
		//さっき作ったファイルにデータを書き込む
		//第１引数：ファイルハンドル
		//第２引数：保存したいデータのアドレス
		//第３引数：保存したいデータのサイズ
		//第４引数：保存する場所（保存が終わると、保存したサイズが自動的に加算される）
		//第５引数：知らん
		WriteFile(hFile, &a[i], sizeof(int), &index, NULL);
	}


	//ファイルを閉じる
	CloseHandle(hFile);


	///////ここまで//////////







	for (int i = 0; i < 3; i++)
	{
		printf("%d\n", a[i]);
	}
	printf("を保存しました\n");

}